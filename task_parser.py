#!/usr/bin/env python3

"""
Author: Sam Windels
Email: sam.windels@gmail.com
Description:
    1. Read data
    3. Run NMTF
    4. Store embedding
"""

import argparse
import numpy as np
import pickle
from PNMTF.pnmtf import PNMTF
from PNMTF.gnmtf import GNMTF
from PNMTF.nmtf_embedding import EmbeddingPNMTF, EmbeddingGNMTF
import scipy
from itertools import product

def dimension_heuristic(n):
    return int(round(np.sqrt(n/2.0)))

def normalize_symmetric(M):
    """
        Divide each entry of an adjacency matrix by square root of the
        rowsum and colsum. Implementation assumes M is symmetric.
    """
    D_array=np.sum(M, axis=1)
    D_array=np.power(D_array,0.5) + np.finfo(float).eps
    M = M / D_array[:,None]
    M = M / D_array[None,:]
    return M

def extract_net_name(adj_file):
    start = max(adj_file.rfind('/')+1, 0)
    end = start  + adj_file[start:].rfind('.')
    if end == -1:
        end = len(adj_file)+1
    net_name = adj_file[start:end]
    return net_name

def main():

    # Initiate the parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-H", "--adjacency_healthy",
                        help="path to edgelist file",
                        required=True)

    parser.add_argument("-C", "--adjacency_cancer",
                        help="path to edgelist file",
                        required=True)

    parser.add_argument("-g", "--genelist",
                        help="genes corresponding the adjacency matrices' rows",
                        required=True)

    parser.add_argument("-p", "--pathway2genesfile",
                        help="tab delimited, rows are a pathway id and list of genes",
                        required=True)

    parser.add_argument("-m", "--model",
                        help="Choose: 'GNMTF' or 'PNMTF'",
                        required=True)

    parser.add_argument("-o", "--outputdir",
                        help="optional, where to store solved NMTF results.\
                              This path should exist",
                        required=True )

    # # Read arguments from the command line
    args = parser.parse_args()
    
    net_name = extract_net_name(args.adjacency_healthy)
    print('Network:', net_name)

    A_healthy = np.asarray(scipy.sparse.load_npz(args.adjacency_healthy).todense())
    A_cancer= np.asarray(scipy.sparse.load_npz(args.adjacency_cancer).todense())
    n = A_healthy.shape[0]
    assert n == A_healthy.shape[1]
    assert A_cancer.shape[0] == A_cancer.shape[1]
    assert n == A_cancer.shape[0]

    genes = [line.strip() for line in open(args.genelist)]
    gene_set = set(genes)
    assert n == len(genes)

    print(' Reading pathway 2 genes')
    pathway_2_genes = {}
    with open (args.pathway2genesfile) as istr:
        for line in istr:
            line = line.strip().split(',')
            pathway = line[0]
            genes_in_path = line[1:]
            pathway_2_genes[pathway] = genes_in_path
            for gene in genes_in_path:
                if gene not in gene_set:
                    raise ValueError(f'{gene} in pathway {pathway} is not in the network')

    print(' Converting pathway_2_genes to pathway_2_indices')
    gene_2_index = dict(zip(genes, range(n)))
    pathway_2_indices = []
    pathways = []
    for pathway, genes_in_path in pathway_2_genes.items():
        pathway_2_indices.append([gene_2_index[gene]
                                  for gene in genes_in_path])
        pathways.append(pathway)

    print(' Normalizing adjacancy matrices')
    A_healthy = normalize_symmetric(A_healthy)
    A_cancer = normalize_symmetric(A_cancer)

    if args.model =='PNMTF':

        ds = [1] * len(pathway_2_indices)
        k = dimension_heuristic(len(genes))

        nmtf = PNMTF(A_healthy, pathway_2_indices)
        nmtf.run(ds, k, verbose=VERBOSE, max_iter = MAX_ITER, init=INIT, 
                 seed=SEED, no_of_batches=1)

        net_name = extract_net_name(args.adjacency_healthy)
        embedding_fp = f'{args.outputdir}/embedding_PNMTF_{net_name}.pickle'
        embedding = EmbeddingPNMTF(nmtf.Gs,
                                   nmtf.Ss,
                                   nmtf.V,
                                   pathways,
                                   nmtf.pathway_2_indices,
                                   pathway_2_genes,
                                   genes
                                   )
        embedding.save(embedding_fp)


        nmtf = PNMTF(A_cancer, pathway_2_indices, V=nmtf.V)
        nmtf.run(ds, 
                 k, 
                 verbose=VERBOSE, 
                 max_iter=MAX_ITER,
                 init=INIT, 
                 seed=SEED,
                 no_of_batches=1,
                 update_V=False)

        net_name = extract_net_name(args.adjacency_cancer)
        embedding_fp = f'{args.outputdir}/embedding_PNMTF_{net_name}.pickle'
        embedding = EmbeddingPNMTF(nmtf.Gs,
                                   nmtf.Ss,
                                   nmtf.V,
                                   pathways,
                                   nmtf.pathway_2_indices,
                                   pathway_2_genes,
                                   genes)
        embedding.save(embedding_fp)

    elif args.model =='GNMTF':

        k = dimension_heuristic(len(genes))

        nmtf = GNMTF(A_healthy)
        nmtf.run(k, verbose=VERBOSE, max_iter = MAX_ITER, init=INIT)

        net_name = extract_net_name(args.adjacency_healthy)
        embedding_fp = f'{args.outputdir}/embedding_GNMTF_{net_name}.pickle'
        embedding = EmbeddingGNMTF(nmtf.G,
                                   nmtf.S,
                                   nmtf.V,
                                   pathways,
                                   pathway_2_indices,
                                   pathway_2_genes,
                                   genes)
        embedding.save(embedding_fp)

    else:
        raise ValueError('{} is an invalid model'.format(model))

if __name__ == "__main__":
    VERBOSE = 1
    MAX_ITER = 1000
    INIT = 'svd'
    SEED = 0
    main()
