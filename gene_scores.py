from nmtf_embedding import load
from itertools import product
import numpy as np
from scipy.spatial.distance import cityblock as manhattan_dist
import pandas as pd


def load_embedding(status, tissue, graphlet):

    netname = f'PPI_{status}_{tissue}_{graphlet}'
    embedding_fp = f'./embeddings/embedding_PNMTF_{netname}.pickle'
    return load(embedding_fp)


def write_gene_scores(pathways, genes, gene_scores, graphlet, score_method, fp):

    df = pd.DataFrame(data=list(zip(pathways, genes, gene_scores)),
                      columns=['pathway', 'entity', 'score'])
    df = df.dropna()
    df['graphlet_adj'] = '$\widetilde{A}_{G_%d}$'  % graphlet
    df['graphlet'] = graphlet
    df['score_method'] = score_method
    df.to_csv(fp, index=False)


def normalize_array(array, norm, axis=0):

    if isinstance(array, list):
        array = np.asarray(array)

    array_not_nan = array[~np.isnan(array)]
    if norm.lower() == 'l2':
        array /= np.linalg.norm(array_not_nan, ord=None, axis=axis)
    elif norm.lower() == 'l1':
        array /= np.linalg.norm(array_not_nan, ord=1, axis=axis)
    elif norm.lower() == 'max':
        array /= np.max(array_not_nan, axis=axis)
    else:
        raise ValueError(f'{norm} is not a valid norm')

    return array


def main():

    tissues = ['lung', 'colon', 'prostate', 'ovary']
    graphlets = range(9)
    norm = 'l2'

    for tissue, graphlet in product(tissues, graphlets):

        pathway_col = []
        genes_col = []
        centralities_col = []
        distances_col = []
        hybrid_col = []

        print(tissue, graphlet, end='\r')
        # Load healthy
        embedding_healthy = load_embedding('healthy', tissue, graphlet)
        # Load cancer
        embedding_cancer = load_embedding('cancer', tissue, graphlet)

        pathways = embedding_healthy.pathways
        pathway_2_indices = embedding_healthy.pathway_2_indices
        for pathway, gene_indices, gene_coords_healthy, gene_coords_cancer in \
                zip(pathways,
                    pathway_2_indices,
                    embedding_healthy.get_genes_in_pathway_2_coordinates(),
                    embedding_cancer.get_genes_in_pathway_2_coordinates()):

            genes = [embedding_healthy.genes[i] for i in gene_indices]

            centralities = np.linalg.norm(gene_coords_healthy, axis=1)
            centralities = normalize_array(centralities, norm)

            distances = [manhattan_dist(gene_coords_healthy[row],
                                        gene_coords_cancer[row])
                         for row in range(len(gene_coords_healthy))]

            distances = normalize_array(distances, norm)

            hybrid = np.sqrt((centralities*distances))
            hybrid = normalize_array(hybrid, norm)

            pathway_col += [pathway] * len(genes)
            genes_col += genes
            centralities_col += centralities.tolist()
            distances_col += distances.tolist()
            hybrid_col += hybrid.tolist()
        
        fp  = f'gene_scores/gene_scores_PNMTF_centrality_{tissue}_{graphlet}.csv'
        write_gene_scores(pathway_col, genes_col, centralities_col, graphlet, 'centrality', fp)

        fp  = f'gene_scores/gene_scores_PNMTF_moving_distance_{tissue}_{graphlet}.csv'
        write_gene_scores(pathway_col, genes_col, distances_col, graphlet, 'moving_distance', fp)
        
        fp  = f'gene_scores/gene_scores_PNMTF_hybrid_{tissue}_{graphlet}.csv'
        write_gene_scores(pathway_col, genes_col, hybrid_col, graphlet, 'hybrid', fp)

if __name__ == '__main__':
    main()
