#!/usr/bin/env python3
"""
File: embedding.py
Author: Sam Windels
Email: sam.windels@gmail.com
Description: Data container used to store NMTF results
"""

import pickle
import numpy as np
from abc import ABC, abstractmethod


def load(fp):
    return pickle.load(open(fp, 'rb'))


class Embedding(ABC):

    """Data container NMTF embedding"""

    def __init__(self, V, pathways, pathway_2_indices, pathway_2_genes,
                 genes 
                 ):

        assert len(pathways) == len(pathway_2_genes)
        assert len(pathways) == len(pathway_2_indices)
        assert len(genes) == len(V)

        self.V = V
        self.pathways = pathways
        self.pathway_2_genes = pathway_2_genes
        self.genes = genes
        self.pathway_2_indices = pathway_2_indices

    def save(self, fp):
        with open(fp, 'wb') as ostr:
            pickle.dump(self, ostr, protocol=4)

    @abstractmethod
    def get_pathway_2_coordinates(self):
        pass


class EmbeddingPNMTF(Embedding):

    def __init__(self, 
                 Gs, Ss, V, 
                 pathways, 
                 pathway_2_indices, 
                 pathway_2_genes,
                 genes):

        assert len(pathways) == len(Gs)
        assert len(pathways) == len(Ss)

        self.Gs = Gs
        self.Ss = Ss

        super().__init__(V,
                         pathways,
                         pathway_2_indices,
                         pathway_2_genes,
                         genes)

    def get_pathway_2_coordinates(self):

        pathway_2_coordinates = []

        for G, S in zip(self.Gs, self.Ss):
            G = G[np.flatnonzero(np.sum(G, axis=1)), :] # Should be changed to at least x genes expressedj
            if G.shape[0] == 0:
                pathway_2_coordinates.append(np.nan)
            else:
                GS = G @ S
                GS_vector = np.mean(GS, axis=0)
                pathway_2_coordinates.append(GS_vector)

        return pathway_2_coordinates


    def get_genes_in_pathway_2_coordinates(self):

        genes_in_pathway_2_coordinates = []

        for G, S in zip(self.Gs, self.Ss):
            GS = G @ S

            # Set all rows with zero coordinages (i.e. gene is not expressed) to nan
            GS[np.sum(GS,axis=1)==0.0,:]=np.nan
            genes_in_pathway_2_coordinates.append(GS)
        return genes_in_pathway_2_coordinates


class EmbeddingGNMTF(Embedding):

    def __init__(self, G, S, V, pathways, pathway_2_indices, pathway_2_genes,
                 genes
                 ):

        self.G = G
        self.S = S

        super().__init__(V,
                         pathways,
                         pathway_2_indices,
                         pathway_2_genes,
                         genes
                         )


    def get_pathway_2_coordinates(self):

        pathway_2_coordinates = []
        GS = self.G @ self.S
        for i, indices in enumerate(self.pathway_2_indices):
            GS_pathway = GS[indices,:]
            GS_pathway = GS_pathway[np.flatnonzero(np.sum(GS_pathway, axis=1)),:]
            GS_vector = np.mean(GS_pathway, axis=0)
            pathway_2_coordinates.append(GS_vector)
        return pathway_2_coordinates




    # def get_genes_pathway_annotated(self):
    #     genes_pathway_annotated = set()
    #     for genes in self.pathway_2_genes:
    #         genes_pathway_annotated.update(genes)


    # def get_pathway_2_clustering(self):
    #     pathway_2_clustering = []
    #     for i, (genes, G) in  enumerate(zip(self.pathway_2_genes, self.Gs)):
    #         cluster_assignment = np.argmax(G, axis=1)
    #         clustering = [ [] for i in range(G.shape[1])]
    #         for gene, cluster_index in zip(genes, cluster_assignment):
    #             clustering[cluster_index].append(gene)
    #         pathway_2_clustering.append(clustering)
    #     return pathway_2_clustering

    # def get_global_clustering(self):
    #     clustering = [ [] for i in range(self.V.shape[1])]
    #     cluster_assignment = np.argmax(self.V, axis=1)
    #     for gene, cluster_index in zip(self.genes, cluster_assignment):
    #         clustering[cluster_index].append(gene)

    #     return clustering


    # def __get_nr_of_pathways(self):
    #     return len(self.pathways)


    # def get_pathway_2_coordinates(self):
    #     if self.model :
    #         return self.__get_pathway_2_coordinates_pathway_integrated(graphlet)
    #     elif model=='global':
    #         return self.__get_pathway_2_coordinates_global_nmtf(graphlet)
    #     else: 
    #         ValueError("model {} unknown".format(model))
    #     return np.vstack(pathway_2_coordinates)

    # def get_pathway_2_centralities(self, model, randomize=None, normalize=None):

    #     if model=='pathway_integrated':
    #         return self.__get_pathway_2_centralities_pathway_integrated(randomize, normalize)
    #     elif model=='eigen':
    #         return self.__get_pathway_2_centralities_pathway_eigen(randomize, normalize)
    #     else: 
    #         ValueError("model {} unknown".format(model))
    #     return np.vstack(pathway_2_coordinates)

    
    # def __get_pathway_2_centralities_pathway_integrated(self, randomize, normalize):
        
    #     if randomize is None:
    #         randomize = False
    #     if normalize is None:
    #         randomize = False 
        
    #     # assert len(self.Ss) % len(self.Gs) == 0
    #     # nr_integrated_matrices = len(self.Ss)/len(self.Gs)
        
    #     pathway_2_centralities = []
    #     for i, (pathway_genes, G) in enumerate(zip(self.pathway_2_genes, self.Gs)):
    #             # indices_expressed = np.flatnonzero(np.sum(G, axis=1))
    #             # G = G[indices_expressed,:]
    #             # pathway_genes = [pathway_genes[i] for i in indices_expressed]
    #         # lower_i = int(i*nr_integrated_matrices)
    #         # upper_i = int(lower_i + nr_integrated_matrices)
    #         # Ss_matching_G = self.Ss[lower_i:upper_i]
    #         # S_matching_G = np.mean(Ss_matching_G, axis=0) #consensus embedding
    #         # G = G @ S_matching_G 
    #         centralities = np.linalg.norm(G, axis=1)
    #         if normalize:
    #             centralities /= np.linalg.norm(centralities)
    #         if randomize:
    #             np.random.shuffle(centralities)
    #         pathway_2_centralities.append(centralities)
    #     return pathway_2_centralities
    
    # def __get_pathway_2_centralities_pathway_integrated_incl_neighbours(self, randomize, normalize):

    #     print('reading')
    #     mol = 'PPI'
    #     tissue = 'prostate'
    #     net_name = '{}_{}_{}'.format(mol,tissue,'healthy')
    #     GAs_h, G_h, genes_h, gene_2_index_h, pathways_h, pathway_2_indices_h, pathway_2_genes_h = \
    #                 fm.load_dataset_single_network("../data/GA/ga_cancer_analysis/", net_name, dense=True, graphlets=list(range(9)))
   
    #     AV = np.mean(GAs_h, axis=0) @ self.V
    #     print('done reading')

        
    #     if randomize is None:
    #         randomize = False
    #     if normalize is None:
    #         randomize = False 
        
    #     assert len(self.Ss) % len(self.Gs) == 0
    #     nr_integrated_matrices = len(self.Ss)/len(self.Gs)
        
    #     pathway_2_centralities = []
    #     for i, (pathway_genes, G) in enumerate(zip(self.pathway_2_genes, self.Gs)):
    #             # indices_expressed = np.flatnonzero(np.sum(G, axis=1))
    #             # G = G[indices_expressed,:]
    #             # pathway_genes = [pathway_genes[i] for i in indices_expressed]
    #         print(i,'/', len(self.Gs), end='\r')
    #         lower_i = int(i*nr_integrated_matrices)
    #         upper_i = int(lower_i + nr_integrated_matrices)
    #         Ss_matching_G = self.Ss[lower_i:upper_i]
    #         S_matching_G = np.mean(Ss_matching_G, axis=0) #consensus embedding
    #         S_inv = np.linalg.pinv(S_matching_G)
    #         embedding = AV @ S_inv 
    #         # embedding = embedding[self.pathway_2_indices[i]]
    #         # V = self.V[self.pathway_2_indices[i]]
    #         # embedding = V @ S_matching_G.T
           
    #         # basis_norms = np.linalg.norm(G, axis=1)
    #         # embedding /= basis_norms[:,None]

    #         centralities = np.linalg.norm(embedding, axis=1)
    #         if normalize:
    #             centralities /= np.linalg.norm(np.linalg.norm(G, axis=1))
    #         if randomize:
    #             np.random.shuffle(centralities)
    #         pathway_2_centralities.append(centralities)
    #     return pathway_2_centralities

    # def __get_pathway_2_centralities_pathway_eigen(self, randomize, normalize):
    #     if randomize is None:
    #         randomize = False
    #     if normalize is None:
    #         randomize = False 
        
    #     list_of_Gs = []
    #     nr_pathways = len(self.pathways)
    #     assert len(self.Gs) % nr_pathways == 0
    #     nr_graphlets = int(len(self.Gs)/len(self.pathways))
        
    #     for g in range(nr_graphlets):
    #         list_of_Gs.append(self.Gs[g*nr_pathways:(g+1)*nr_pathways])
    #         Gs = np.average(list_of_Gs, axis=0)

    #     pathway_2_centralities = []
    #     for pathway_genes, G in zip(self.pathway_2_genes, Gs):
    #             # indices_expressed = np.flatnonzero(np.sum(G, axis=1))
    #             # G = G[indices_expressed,:]
    #             # pathway_genes = [pathway_genes[i] for i in indices_expressed]
    #         centralities = np.linalg.norm(G, axis=1)
    #         if normalize:
    #             centralities /= np.linalg.norm(centralities)
    #         if randomize:
    #             np.random.shuffle(centralities)
    #         pathway_2_centralities.append(centralities)
    #     return pathway_2_centralities

    # def get_pathway_2_accuracy_scores(self, true_set, model, randomize=None, normalize=None):
        
    #     pathway_2_centralities = self.get_pathway_2_centralities(model, randomize, normalize=normalize)
       
    #     auc_roc_scores = []
    #     auc_pr_scores = []
    #     for pathway_genes, centralities, G in zip(self.pathway_2_genes, pathway_2_centralities, self.Gs):
    #         indices_expressed = np.flatnonzero(np.sum(G, axis=1))
    #         centralities = centralities[indices_expressed]
    #         pathway_genes = [pathway_genes[i] for i in indices_expressed]
            
    #         y_true = []
    #         for gene in pathway_genes:
    #             if gene in true_set:
    #                 y_true.append(1.0)
    #             else:
    #                 y_true.append(0.0)
    #         if np.sum(y_true) != 0.0:
    #             auc_roc = roc_auc_score(y_true, centralities)
    #             auc_pr = average_precision_score(y_true, centralities)
    #         else:
    #             auc_roc = np.nan 
    #             auc_pr= np.nan
                
    #         auc_roc_scores.append(auc_roc)
    #         auc_pr_scores.append(auc_pr)
        
    #     return auc_roc_scores, auc_pr_scores



    # def get_condensed_pathway_2_pathway_distance_matrix(self, model, metric):

    #     coordinates = self.get_pathway_2_coordinates(model)

    #     # distance_matrix_integrated = scipy.spatial.distance.squareform(scipy.spatial.distance.pdist(coordinates_integrated, metric=metric))
    #     # if metric is None:
    #         # metric = 'cosine'
    #     condensed_dist_matrix = scipy.spatial.distance.pdist(coordinates, metric=metric)
    #     return condensed_dist_matrix

    # def __shuffle_matrix_rows(self, M):
    #     order = np.arange(M.shape[0])
    #     np.random.shuffle(order)
    #     return M[order,:]

    # def get_distances_from_pathway_centroids(self, metric):
    #     distances_genes_to_pathways= []
    #     pathway_coordinates = self.get_pathway_2_coordinates('pathway_integrated', -1)
    #     # for pathway_coordinate in pathway_coordinates:
    #         # gene_distances = []
    #         # print(pathway_coordinate)
    #         # for row in self.V: 
    #         #     print(row)
    #             # gene_distance = scipy.spatial.distance.cdist(pathway_coordinate, row, metric=metric)
    #             # gene_distances.append(gene_distance)
    #         # gene_distances = scipy.spatial.distance.cdist([pathway_coordinate], self.V, metric=metric)
    #         # distances_genes_to_pathways.append(gene_distances)
    #     return scipy.spatial.distance.cdist(pathway_coordinates, self.V, metric=metric)

def main():
    embedding = load('embeddings/embedding_pathway_integrated_PPI_prostate_cancer_8.pickle')
    embedding.get_pathway_2_coordinates()

if __name__ == "__main__":
    main()


