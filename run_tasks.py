

import subprocess
import argparse
import pandas as pd

def main():

    parser = argparse.ArgumentParser()

    parser.add_argument("-tf", "--task_file",
                        help="path to task_file",
                        required = True)

    args = parser.parse_args()

    df = pd.read_csv(args.task_file, index_col='task_id')

    # task = df.loc[args.task_id]

    for task_id, task in df.iterrows(): 
        subprocess.call(['python', 'run_task.py',
                         '-H', task.adjacency_healthy,
                         '-C', task.adjacency_cancer,
                         '-g', task.genelist,
                         '-p', task.pathway2genesfile,
                         '-m', task.model,
                         '-o', task.outputdir
                         ])

if __name__ == "__main__":
    main()
