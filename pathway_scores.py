from nmtf_embedding import load
from itertools import product
import numpy as np
from scipy.spatial.distance import cityblock as manhattan_dist
import pandas as pd


def load_embedding(status, tissue, graphlet):

    netname = f'PPI_{status}_{tissue}_{graphlet}'
    embedding_fp = f'./embeddings/embedding_PNMTF_{netname}.pickle'
    return load(embedding_fp)


def write_pathway_scores(pathways, pathway_scores, graphlet, score_method, fp):

    df = pd.DataFrame(data=list(zip(pathways, pathway_scores)),
                      columns=['entity', 'score'])
    df = df.dropna()
    df = df.sort_values(by=['score'], ascending=False)
    df['rank'] = df['score'].rank(ascending = False)
    df['graphlet'] = graphlet
    df['graphlet_adj'] = '$\widetilde{A}_{G_%d}$' % graphlet
    df['score_method'] = score_method
    df.to_csv(fp, index=False)


def normalize_array(array, norm, axis=0):

    if isinstance(array,  list):
        array = np.asarray(array)

    array_not_nan = array[~np.isnan(array)]
    if norm.lower() == 'l2':
        array /= np.linalg.norm(array_not_nan, ord=None ,axis=axis)
    elif norm.lower() == 'l1':
        array /= np.linalg.norm(array_not_nan, ord=1 ,axis=axis)
    elif norm.lower() == 'max':
        array /= np.max(array_not_nan, axis=axis)
    else:
        raise ValueError(f'{norm} is not a valid norm')

    return array


def main():

    tissues = ['lung', 'colon', 'prostate', 'ovary']
    graphlets = range(9)
    norm = 'l1'

    for tissue, graphlet in product(tissues, graphlets):
        print(tissue, graphlet, end='\r')
        # Load healthy
        embedding_healthy = load_embedding('healthy', tissue, graphlet)
        # Load cancer
        embedding_cancer = load_embedding('cancer', tissue, graphlet)
        
        # assertListEqual(embedding_healthy.pathways, embedding_cancer.pathways) 

        pathway_coords_healthy = embedding_healthy.get_pathway_2_coordinates()
        pathway_coords_cancer = embedding_cancer.get_pathway_2_coordinates()

        centralities = np.linalg.norm(pathway_coords_healthy, axis=1)

        centralities = normalize_array(centralities, norm)

        moving_distances = [manhattan_dist(pathway_coords_healthy[row],
                                           pathway_coords_cancer[row])
                            for row in range(len(embedding_healthy.pathways))]

        moving_distances = normalize_array(moving_distances, norm)

        hybrid_scores = []
        for centrality, distance in zip(centralities, moving_distances):
            if ~np.isnan(distance):
                hybrid_scores.append(np.sqrt(centrality*distance))
            else:
                hybrid_scores.append(np.nan)
        hybrid_scores = normalize_array(hybrid_scores, norm)

        pathways = embedding_healthy.pathways

        fp = f'pathway_scores/pathway_scores_PNMTF_centrality_{tissue}_{graphlet}.csv'
        write_pathway_scores(pathways, centralities, graphlet, 'centrality', fp)

        fp = f'pathway_scores/pathway_scores_PNMTF_moving_distance_{tissue}_{graphlet}.csv'
        write_pathway_scores(pathways, moving_distances, graphlet, 'moving_distance', fp)

        fp = f'pathway_scores/pathway_scores_PNMTF_hybrid_{tissue}_{graphlet}.csv'
        write_pathway_scores(pathways, hybrid_scores, graphlet, 'hybrid', fp)


if __name__ == '__main__':
    main()
