import numpy as np
import json
import time
from scipy.sparse.linalg import eigsh
from scipy.linalg import eigh

""" 
    
"""


class GNMTF(object):

    def __init__(self, A):
        """

        Parameters:
        -----------

        As : list(2d np.ndarray(float))
            List of n X n dimensional adjacency matrices.

        G : 2d np.ndarray(float)
            n X k dimensional latent matrix.

        Ss : (2d np.ndarray(float)
            k X k dimensional latent matrix.

        """
        self.A = A
        assert A.shape[0] == A.shape[1]
        self.n = self.A.shape[0]

        self.G = None
        self.Ss = []
        self.eps_init = 1e-5
        self.eps = 1e-10



    def __init_G_and_S(self, init, k):
        """ Handles initialisation of self.G when starting a run 

        Parameters:
        ----------


        init_G : str
            Lowercase string describing the initialisation strategy to be used.
            Options: svd or random.

        """
        print('init G') 
        
        if init == 'svd':
            G = []
            S = []
            eigen_values, eigen_vectors = eigh(self.A,
                                               eigvals=(self.n-k,self.n-1)
                                               )
            eigen_vectors_pos, eigen_vectors_neg = self._get_pos_neg(eigen_vectors)
            for i, (pos_eigenvector, neg_eigenvector) in enumerate(zip(eigen_vectors_pos.T, eigen_vectors_neg.T)):
                norm_pos = np.sqrt(pos_eigenvector @ pos_eigenvector)
                norm_neg = np.sqrt(neg_eigenvector @ neg_eigenvector)
                # print(norm_pos)
                if norm_pos>norm_neg:
                    G.append(pos_eigenvector)
                else:
                    G.append(neg_eigenvector)
                S_col = np.zeros((k,1))
                S_col[i]=abs(eigen_values[i])
                S.append(S_col)
            S=np.hstack(S)
            G=np.vstack(G).T

        elif init == 'random':
            G = np.random.rand(self.n, k)
            S = np.random.rand(k, k)

        else:
            raise Exception(f"{init} not a known initialisation strategy for G\
                            choose from: 'svd' or 'random'.")
        G += self.eps_init
        S += self.eps_init

        self.G = G
        self.S = S

    def run(self, k, init=None, max_iter=None, max_time=None, verbose=None):


        """ Run NMTF fusion algorithm

        Parameters:
        -----------


        Returns:
        --------

        """

        if init is None:
            init = "nndsvd"
        if max_iter is None:
            max_iter = 500
        if max_time is None:
            max_time = np.inf
        if verbose is None:
            verbose=1


        info = {'k': k,
                'alg': str(self.__class__),
                'max_iter': max_iter if max_iter is not None else self._default_max_iter,
                'verbose': verbose,
                'max_time': max_time if max_time is not None else self._default_max_time,
                'init': init,
                'n' : self.n
                }
        self.__init_G_and_S(init, k)

        if verbose >= 1:
            print('[NMTF] Running: ')
            print(json.dumps(info, indent=4, sort_keys=True))

        total_time = 0
        

        start = time.time()

        if verbose >= 3:
            norm_A, abs_error, rel_error = self.compute_error()
            print('initialization: rel_error: {} abs_error: {}'.format(rel_error, abs_error))
        for i in range(1, info['max_iter'] + 1):
            start_iter = time.time()
            try:
                self.__iter_solver(k, i)
            except np.linalg.LinAlgError as e:
                break
            elapsed = time.time() - start_iter
            if verbose == 1:
                if i % 100 == 0:
                    print(i)
            elif verbose >= 2:
                norm_A, abs_error, rel_error = self.compute_error()
                print(i)
                if verbose >= 3:
                    print('iter: {} elapsed t: {} rel_error: {} abs_error: {}'.\
                          format(i, elapsed, rel_error, abs_error))

            total_time += elapsed
            if total_time > info['max_time']:
                break
        norm_A, abs_error, rel_error = self.compute_error()
        final = {}
        final['norm_A'] = norm_A
        final['rel_error'] = rel_error
        final['iterations'] = i
        final['elapsed'] = time.time() - start

        rec = {'info': info, 'final': final}

        if verbose >= 1:
            print('[NMF] Completed: ')
            print(json.dumps(final, indent=4, sort_keys=True))
        # return (Ws, H, rec)

    def compute_error(self):

        abs_error = np.linalg.norm(self.A - (self.G @ self.S @ self.G.T))
        norm_A = np.linalg.norm(self.A)
        rel_error = abs_error / norm_A

        return norm_A, abs_error, rel_error


    def __iter_solver(self, k, iter):

        ##update G 
        Ge = np.zeros((self.n, k))
        Gd = np.zeros((self.n, k))
        AGS = self.A @ (self.G @ self.S)
        AGS_pos, AGS_neg = self._get_pos_neg(AGS)

        GSS = self.G @ (self.S @ self.S)
        GSS_pos, GSS_neg = self._get_pos_neg(GSS)

        Ge += AGS_pos + GSS_neg
        Gd += AGS_neg + GSS_pos
        Gd += self.eps
        self.G *= np.sqrt(Ge/Gd)


        #update S
        # GtG = self.G.T @ self.G
        GtAG = self.G.T @ self.A @ self.G
        GtAG_pos, GtAG_neg = self._get_pos_neg(GtAG)

        S_pos, S_neg =self._get_pos_neg(self.S)

        Se = GtAG_pos + S_neg
        Sd = GtAG_neg + S_pos+ self.eps

        # GtGSGtG = GtG @ S @ GtG
        # GtGSGtG_pos, GtGSGtG_neg =self._get_pos_neg(GtGSGtG)
        # Se = GtAG_pos + GtGSGtG_neg
        # Sd = GtAG_neg + GtGSGtG_pos+ self.eps

        self.S *= np.sqrt(Se/Sd)

    def _get_pos_neg(self, X):
        """
        For a given matrix X, return a pair of matrices with the absolute
        values of the positive/negative values of X respectively.

        :X: 2d np.ndarray(float)
        :returns: (2d np.ndarray, 2d np.ndarray)

        """
        abs = np.abs(X)
        return (abs+X)/2, (abs-X)/2


def main():


    from scipy.sparse import random
    import copy
    import networkx as nx
    """
    main used for dev purposes

    """

    # G = nx.read_edgelist("../../ksub_ga/data/networks/PPI_human.edgelist")
    # GAs = [ np.asarray(nx.to_numpy_matrix(G))]
    # n = 5000
    # density = 0.02
    # A = random(n, n, density=density,  format='csr')
    # A = np.asarray(A.todense())
    G = nx.read_edgelist("../../../data/networks/PPI_union_yeast_LCM.edgelist")
    A =  np.asarray(nx.to_numpy_matrix(G))
    n = G.number_of_nodes()
    k = int(round(np.sqrt(n/2.0)))
    nmtf = GNMTF(A)
    nmtf.run(k, verbose=3, max_iter = 1000, init='svd')



#
if __name__ == "__main__":

    main()
