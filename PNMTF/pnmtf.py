#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
File: nmtf_integrate_lowmem.py
Author: Sam Windels
Email: sam.windels@gmail.com
Github: https://gitlab.com/sam.windels
Description:
    For an adjacency matrix and a list of indices present in a
    pathway(corresponding to rows and colls in A, representing genes), compute
    one integrated space V and for one integrated G per pathway.
"""




import numpy as np
import json
import time
import sys
import argparse
from scipy import sparse
from scipy.sparse.linalg import eigsh, svds
from scipy.linalg import eigh, svd
import pandas as pd


# set the threading layer before any parallel target compilation

""" 
    MF based data integration by Sam Windels, sam.windels@gmail.com.
    code is an addatption from https://github.com/kimjingu/nonnegfac-python
"""



class PNMTF(object):
 
    def __init__(self, A, pathway_2_indices, Gs=None, Ss=None, V=None):
        """

        Parameters:
        -----------

        A : 2d np.ndarray(float)
            n X n dimensional adjacency matrix.

        Gs : list(2d np.ndarray(float), optional)
            List of n X k dimensional latent matrices. G initialisation
            pre-computed.

        Ss : list(2d np.ndarray(float), optional)
            List of latent matrices. In case comes pre-initalized.

        V : 2d np.ndarray(float), optional)
            n X k dimensional latent matrix. V initialisation
            pre-computed.

        """

        self.Gs = Gs
        self.Ss = Ss
        self.V = V

        self.A = A
        assert A.shape[0] == A.shape[1]
        self.n = self.A.shape[1]

        self.pathway_2_indices = pathway_2_indices
        self.no_of_pathways = len(pathway_2_indices)

        self.eps_init = 1e-5 #  Used for G/S/V init
        self.eps = 1e-10 #  Used in MUR

    def __init_random(self, ds, k):

        if self.Gs is None:
            self.Gs = []
            for path_index in range(self.no_of_pathways):
                d = ds[path_index]
                m = len(self.pathway_2_indices[path_index])
                G = np.random.rand(m, d) + self.eps_init
                self.Gs.append(G)

        if self.Ss is None:
            self.Ss = []
            for path_index in range(self.no_of_pathways):
                d = ds[path_index]
                S = np.random.rand(d, k) + self.eps_init
                self.Ss.append(S)

        if self.V is None:
            self.V = np.random.rand(self.n, k) + self.eps_init


    def __init_svd(self, ds, k):
        """
        Use SVD to initialize PNMTF. Inspired by:

        Boutsidis, C., & Gallopoulos, E. (2008). SVD based initialization: A
        head start for nonnegative matrix factorization. Pattern recognition,
        41(4), 1350-1362.

        """

        init_Ss = self.Ss is None
        init_Gs = self.Gs is None

        if init_Ss:
            self.Ss = []

        if init_Gs:
            self.Gs = []

        # If self.Ss or self.Gs is an empty list, initialize
        if init_Ss or init_Gs:
            for path_index in range(self.no_of_pathways):
                d = ds[path_index]
                self.__load_P_into_memory(path_index)
                # careful, largest variance (K) ,eivenvectors (U,V) sorted ascending
                # -> reverse ordering first
                U, K, _ = svds(self.P, d, which='LM')
                K_indices_sorted = np.argsort(-K)
                U = U[:,K_indices_sorted]
                K = K[K_indices_sorted]

                if init_Ss:
                    S = np.zeros((d, k))
                    for diag_index in range(min(d,k)):
                        S[diag_index, diag_index] = K[diag_index]
                        S += self.eps_init
                        self.Ss.append(S)

                if init_Gs:
                    G = self.__get_max_norm_positive_matrix(U, d)
                    G += self.eps_init
                    self.Gs.append(G)

        if self.V is None:
            max_d = max(ds)
            max_dim = max([max_d, k])

            # careful, largest variance (K) ,eivenvectors (U,V) sorted ascending
            # -> reverse ordering first
            U, K, _ = svds(self.A, max_dim, which='LM')
            K_indices_sorted = np.argsort(-K)

            U = U[:,K_indices_sorted]
            K = K[K_indices_sorted]
            V = self.__get_max_norm_positive_matrix(U, k)
            V += self.eps_init
            self.V = V


    def __get_max_norm_positive_matrix(self, X, nr_cols):
        """
        Helper function. For 'nr_cols' columnvectors of X:
        Get the pair of vectors + and -:
        + = a vector with all the positive entries of X and negative entries
        set to 0.
        - = a vector with all positive entries set to 0 and the absolute valies
          of the negative values in x.
        Keep + or -, depending which has the heighest norm.  """

        X_max_norm = np.zeros((X.shape[0], nr_cols))
        X_pos, X_neg = self.__get_pos_neg(X)
        for col_i in range(nr_cols):
            X_pos_col = X_pos[:,col_i]
            X_neg_col = X_neg[:,col_i]
            norm_X_pos_col = np.linalg.norm(X_pos_col)
            norm_X_neg_col = np.linalg.norm(X_neg_col)

            if norm_X_pos_col > norm_X_neg_col:
                X_max_norm[:,col_i] = X_pos_col + self.eps
            else:
                X_max_norm[:,col_i] = X_neg_col + self.eps
        return X_max_norm

    def __init_norm_A(self):

        """
        Store the norm of A so that we don't have to recompute it.

        """
        norm_A = 0.0
        for path_index in range(self.no_of_pathways):
            self.__load_P_into_memory(path_index)
            norm_A += np.linalg.norm(self.P)
        self.__norm_A = norm_A

    def run(self, ds, k, init='svd', max_iter=200,
            max_time=np.inf, verbose=1, update_Gs=None,
            update_Ss=None, update_V=None, seed=None, no_of_batches=None):
        
        """ Run NMTF fusion algorithm

        Parameters:
        -----------


        Returns:
        --------

        """
        if update_Gs is None:
            update_Gs = True

        if update_Ss is None:
            update_Ss = True

        if update_V is None:
            update_V = True

        if no_of_batches is None:
            no_of_batches = 1

        info = {'k': k,
                'alg': str(self.__class__),
                'max_iter': max_iter if max_iter is not None else self._default_max_iter,
                'verbose': verbose,
                'max_time': max_time if max_time is not None else self._default_max_time,
                'init': init,
                'n' : self.n,
                'update_Gs': update_Gs,
                'update_Ss': update_Ss,
                'update_V': update_V,
                'seed': seed
                }

        if seed:
            np.random.seed(seed)
        if init == 'svd':
            self.__init_svd(ds, k)
        elif init == 'random':
            self.__init_random(ds, k)
        else:
            raise ValueError("{} is not a valid value for init, \
                             choose 'svd' or 'random'.".format(init))

        self.__progress_log = []
        self.__init_norm_A()

        print('done initialising')
        if verbose >= 1:
            print('[NMTF] Running: ')
            print(json.dumps(info, indent=4, sort_keys=True))

        total_time = 0

        if verbose >= 2 :
            his = {'iter': [], 'elapsed': [], 'rel_error': []}

        start = time.time()

        for i in range(1, info['max_iter'] + 1):
            start_iter = time.time()
            try:
                self.__iter_solver( update_Gs, update_Ss, update_V, no_of_batches)
            except np.linalg.LinAlgError as e:
                break
            elapsed = time.time() - start_iter
            if verbose == 1 and i % 10 ==0:    
                    print('iter: {} elapsed t: {} '.format(i, elapsed))
            elif verbose == 2:
                print('iter: {} elapsed t: {} '.format(i, elapsed))
            elif verbose == 3 or (verbose>3 and i % verbose==0):
                    abs_error, rel_error = self.__compute_error()
                    print('iter: {} elapsed t: {} rel_error: {} abs_error: {}'.\
                          format(i, elapsed, rel_error, abs_error))

                    self.__progress_log.append((i, elapsed, self.__norm_A, abs_error, rel_error))

            total_time += elapsed
            if total_time > info['max_time']:
                break
        if verbose >= 1:
            abs_error, rel_error = self.__compute_error()
            final = {}
            final['norm_A'] = self.__norm_A
            final['rel_error'] = rel_error
            final['iterations'] = i
            final['elapsed'] = time.time() - start

            rec = {'info': info, 'final': final}

            print('[NMF] Completed: ')
            print(json.dumps(final, indent=4, sort_keys=True))

    def get_progress_log(self):
        return pd.DataFrame.from_records(self.__progress_log,
               columns=['iteration','time_iteration','norm_As','abs_error','rel_error'])

    def __compute_error(self):

        abs_error=0.0
        for path_index in range(self.no_of_pathways):

            self.__load_P_into_memory(path_index)
            P, G, S, V = self.__get_PGSV(path_index)

            abs_error += np.linalg.norm(P - (G @ S @ V.T))
        rel_error = abs_error / self.__norm_A

        return abs_error, rel_error

    def __get_PGSV(self, path_index):

        P = self.P
        G = self.Gs[path_index]
        S = self.Ss[path_index]
        V = self.V
        return P, G, S, V

    def __update_S(self, path_index):

        P, G, S, V = self.__get_PGSV(path_index)

        GtPV = (G.T @ P) @ V
        GtPV_pos, GtPV_neg = self.__get_pos_neg(GtPV)

        if G.shape[1] != S.shape[0]:
            print(G.shape, S.shape)
        # GtGS = G.T @ (G @ S)
        # GtGS_pos, GtGS_neg = self.__get_pos_neg(GtGS)

        # Se = GtPV_pos + GtGS_neg
        # Sd = GtPV_neg + GtGS_pos+ self.eps

        # S_pos, S_neg = self.__get_pos_neg(S)
        GtGS = (G.T @ G) @ S
        GtGS_pos, GtGS_neg = self.__get_pos_neg(GtGS)

        Se = GtPV_pos + GtGS_neg
        Sd = GtPV_neg + GtGS_pos + self.eps

        self.Ss[path_index] *= np.sqrt(Se/Sd)


    def __update_G(self, path_index):

        P, G, S, V = self.__get_PGSV(path_index)

        PVSt = P @ (V @ S.T)
        PVSt_pos, PVSt_neg = self.__get_pos_neg(PVSt)

        GSSt = G @ (S  @ S.T)
        GSSt_pos, GSSt_neg = self.__get_pos_neg(GSSt)

        Ge = PVSt_pos + GSSt_neg
        Gd = PVSt_neg + GSSt_pos + self.eps
        self.Gs[path_index] *= np.sqrt(Ge/Gd)

    def __compute_gradient_V(self, path_index):

        P, G, S, V = self.__get_PGSV(path_index)

        PtGS = P.T @ G @ S
        PtGS_pos, PtGS_neg = self.__get_pos_neg(PtGS)

        GtG = G.T @ G
        VStGtGS = (V @ S.T) @ (GtG @ S)
        VStGtGS_pos, VStGtGS_neg = self.__get_pos_neg(VStGtGS)

        Ve = PtGS_pos+ VStGtGS_neg
        Vd = PtGS_neg + VStGtGS_pos
        return Ve, Vd

    def __update_batch(self, path_indices_batch, update_Gs, update_Ss, update_V):

        if update_V:
            Ve = np.zeros(self.V.shape)
            Vd = np.zeros(self.V.shape)

        for path_index in path_indices_batch:

            #load submatrix of A for pathway into memory
            self.__load_P_into_memory(path_index)

            if update_Gs:
                G = self.__update_G(path_index)
            if update_Ss:
                S = self.__update_S(path_index)
            if update_V:
                Ve_update, Vd_update = self.__compute_gradient_V(path_index)
                Ve += Ve_update
                Vd += Vd_update

        if update_V:
            Vd += self.eps
            self.V *= np.sqrt(Ve/Vd)

    def __iter_solver(self, update_Gs, update_Ss, update_V, no_of_batches):

        """
        Update all Gs, Ss and V iterating over all pathways once.

        If 'no_of_batches' >1, then the latent matrix 'self.V' is update
        multiple times, each time training on a random subset of all pathways
        in each batch.

        """

        path_indices_shuffled = np.arange(len(self.pathway_2_indices))

        if no_of_batches >1:  # No shuffle if training on all pathways.
            np.random.shuffle(path_indices_shuffled)

        batch_size = int(round(len(self.pathway_2_indices)/float(no_of_batches)))
        for batch_i in range(no_of_batches):
            if batch_i == no_of_batches-1:
                path_indices_batch = path_indices_shuffled[batch_i*batch_size:]
            else:
                path_indices_batch = path_indices_shuffled[batch_i*batch_size : (batch_i+1)*batch_size]

            self.__update_batch(path_indices_batch, update_Gs, update_Ss, update_V)

    def __get_pos_neg(self, X):
        """
        For a given matrix X, return a pair of matrices with the absolute
        values of the positive/negative values of X respectively.

        :X: 2d np.ndarray(float)
        :returns: (2d np.ndarray, 2d np.ndarray)

        """
        abs = np.abs(X)
        return (abs+X)/2, (abs-X)/2

    def __load_P_into_memory(self, path_index):

        """ induces the pathway onto the graphlet adjacency matrix """
        pathway_indices = self.pathway_2_indices[path_index]
        self.P = self.A[pathway_indices, :]

def main():


    """
    main used for dev purposes

    """

    from scipy.sparse import random

    n = 1000 # number of nodes in the network
    density =0.05
    no_of_pathways = 100
    k = int(round(np.sqrt(n/2)))

    A = random(n, n, density=density,  format='csr')
    A = np.asarray(A.todense())

    pathway_2_indices = []
    ds = []
    for _ in range(no_of_pathways):
        path_size = np.random.randint(10,50)
        pathway_2_indices.append(sorted(np.random.choice(range(n), size=path_size)))
        # ds.append(int(np.round(np.sqrt(path_size/2.0))))
        ds.append(1)

    nmtf = PNMTF(A, pathway_2_indices)
    nmtf.run(ds, k, verbose=3, max_iter = 100, init='svd', no_of_batches=1)
    print(nmtf.get_progress_log())


if __name__ == "__main__":

    main()
